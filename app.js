const express = require("express");
const multer = require("multer");
const fs = require("fs");


let htmlBeg = `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<h1>KenzieGram</h1>
<form action="/upload" enctype="multipart/form-data" method="POST">
    <input type="file" name="my-file">
    <button type="submit">Upload your file</button>
</form>
<br>
    <div id="pictures">`

let htmlEnd = ` 
    </div>
</body>
</html>`

let htmlUploadedPictureBeg = `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<h1>KenzieGram</h1>
<h2>Photo Uploaded</h2>
<a href="/" id="backButton">Go Back</a>
`
let htmlUploadedPictureEnd = `
</body>
</html>
`




const port = 3000;
const publicPath = ('public/')

const app = express();
app.use(express.json())
app.use(express.static(publicPath))
const upload = multer({dest: publicPath + "uploads/"});

const uploadedFiles = [];

app.post('/upload', upload.single('my-file'), function (request, response, next) {
    // request.file is the `myFile` file
    // request.body will hold the text fields, if there were any
    uploadedFiles.push(request.file.filenames);
    let uploadedPicture = pictureToIMGtag(request.file.filename);
    let html = htmlUploadedPictureBeg + uploadedPicture + htmlUploadedPictureEnd;
    response.end(html);
  })

app.get('/', (req, res) => {

    const path = './public/uploads';
    fs.readdir(path, function (err, items) {
        items.splice(items.findIndex(item => item === '.DS_Store'),1)
        items.sort((a,b) => {
            return fs.statSync(path + '/'+ a).mtimeMs - fs.statSync(path + '/' + b).mtimeMs
        })
        let pictures = items.map(picture => pictureToIMGtag(picture)).join('');
        let html = htmlBeg + pictures + htmlEnd
        res.send(html);
    })
})

function pictureToIMGtag(filename){
    return `<div class="imageDiv"><img src="uploads/${filename}" class="image"></div>`
}


app.listen(port);

